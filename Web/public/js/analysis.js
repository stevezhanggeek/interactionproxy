(function($) {
  var data = {};

  var analyzeWindow = function(packageName, windowTreeId) {
    var windowTreeNodeRef = firebase.database().ref("WindowTreeNodeDB")
    var windowTreeRef = firebase.database().ref("WindowTreeDB").child(windowTreeId);
    windowTreeRef.once('value').then(function(windowTreeSnapshot) {
      windowTreeNodeRef.once('value').then(function(windowTreeNodeSnapshot) {
        var talkbackNodeIdList = windowTreeSnapshot.child("talkbackWindowTreeNodeIdList");
        var talkbackNodeIdListSize = talkbackNodeIdList.numChildren();
        console.log(talkbackNodeIdListSize);
        if (talkbackNodeIdListSize == 0) {
          talkbackNodeIdListSize = 1 // In case 0/0
        }

        var hasViewIdCount = 0;
        var hasLabelCount = 0;
        var canLabelByTalkbackCount = 0;

        var windowTreeNode = windowTreeNodeSnapshot.child(windowTreeId);
        
        talkbackNodeIdList.forEach(function(talkbackNodeId) {
          talkbackNode = windowTreeNode.child(talkbackNodeId.val());
          
          if (talkbackNode.hasChild("viewIdResName")) {
            hasViewIdCount++;

            if (talkbackNode.child("className").val() == "android.widget.ImageView"
             || talkbackNode.child("className").val() == "android.widget.ImageButton") {
              if (!(talkbackNode.hasChild("text") && talkbackNode.child("text").val().length != 0)
               && !(talkbackNode.hasChild("contentDescription") && talkbackNode.child("contentDescription").val().length != 0)) {
                canLabelByTalkbackCount++;
              }
            }
          }

          if (talkbackNode.hasChild("talkbackText") && talkbackNode.child("talkbackText").val() != "") {
            hasLabelCount++;
          }

        });

        console.log("hasViewId%: " + 100*hasViewIdCount/talkbackNodeIdListSize + "%");
        console.log("hasLabel%: " + 100*hasLabelCount/talkbackNodeIdListSize + "%");
        console.log("canLabelByTalkback%: " + 100*canLabelByTalkbackCount/talkbackNodeIdListSize + "%");

        if (!(packageName in data)) {
          data[packageName] = []
        }
        data[packageName].push({
          windowTreeId: windowTreeId,
          hasViewIdCount: hasViewIdCount,
          hasLabelCount: hasLabelCount,
          canLabelByTalkbackCount: canLabelByTalkbackCount,
          talkbackNodeIdListSize: talkbackNodeIdListSize
        })
      });
    });
  }

  var showResult = function() {
    console.log(data);

    $("#resultTable tr").remove();
    var table = document.getElementById("resultTable");
    var titleList = ["packageName", "# of screen", "# of talkbackNode", "# hasViewId", "# hasLabel", "# canLabelByTalkback"];
    var header = table.createTHead();
    var headerRow = header.insertRow(0);
    for (var i = 0; i < titleList.length; i++) {
        headerRow.insertCell(i).outerHTML = "<th>" + titleList[i] + "</th>";
    }
    
    Object.keys(data).forEach(function(packageName) {
      console.log(data[packageName]);

      var row = table.insertRow(-1);
      var packageNameCell = row.insertCell(0);
      packageNameCell.innerHTML = packageName;
      var screenCountCell = row.insertCell(1);
      screenCountCell.innerHTML = data[packageName].length;

      var talkbackNodeIdListSize = 0;
      var hasViewIdCount = 0;
      var hasViewIdPercent = 0.0;
      var hasLabelCount = 0;
      var hasLabelPercent = 0.0;
      var canLabelByTalkbackCount = 0;
      var canLabelByTalkbackPercent = 0.0;

      data[packageName].forEach(function(screenData) {
        talkbackNodeIdListSize += screenData["talkbackNodeIdListSize"];
        hasViewIdCount += screenData["hasViewIdCount"];
        hasViewIdPercent += (screenData["hasViewIdCount"]/screenData["talkbackNodeIdListSize"]);
        hasLabelCount  += screenData["hasLabelCount"];
        hasLabelPercent += (screenData["hasLabelCount"]/screenData["talkbackNodeIdListSize"]);
        canLabelByTalkbackCount += screenData["canLabelByTalkbackCount"];
        canLabelByTalkbackPercent += (screenData["canLabelByTalkbackCount"]/screenData["talkbackNodeIdListSize"]);
      });

      hasViewIdPercent /= data[packageName].length;
      hasLabelPercent  /= data[packageName].length;
      canLabelByTalkbackPercent /= data[packageName].length;

      var talkbackNodeCell = row.insertCell(2);
      var hasViewIdCell = row.insertCell(3);
      var hasLabelCell = row.insertCell(4);
      var canLabelByTalkbackCell = row.insertCell(5);
      talkbackNodeCell.innerHTML       = talkbackNodeIdListSize;
      hasViewIdCell.innerHTML          = (100*hasViewIdCount/talkbackNodeIdListSize).toFixed(1) + "% | " + (100*hasViewIdPercent).toFixed(1) + "%";
      hasLabelCell.innerHTML           = (100*hasLabelCount/talkbackNodeIdListSize).toFixed(1)  + "% | " + (100*hasLabelPercent).toFixed(1)  + "%";;
      canLabelByTalkbackCell.innerHTML = (100*canLabelByTalkbackCount/talkbackNodeIdListSize).toFixed(1) + "% | " + (100*canLabelByTalkbackPercent).toFixed(1) + "%";;
    });

  }

  $('#runButton').click(function() {
    data = {};

    var appIdRef = firebase.database().ref("AppIdDB");
    appIdRef.once('value').then(function(snapshot) {
      snapshot.forEach(function(packageName) {
        console.log(packageName.key);
        packageName.forEach(function(version) {
          version.child("appActivityList").forEach(function(appActivity) {
            console.log(appActivity.key);
            appActivity.forEach(function(appWindowTreeId) {
              console.log(appWindowTreeId.key);
              analyzeWindow(packageName.key, appWindowTreeId.key);
            });
          });
        });
      });
    });

    setTimeout(showResult, 2000);
  });

  $('#resultButton').click(showResult);

})(jQuery);