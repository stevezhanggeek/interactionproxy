package xiaoyiz.InteractionProxies.Capture

import android.accessibilityservice.AccessibilityService
import android.content.Context
import android.util.Log
import android.view.KeyEvent
import android.view.accessibility.AccessibilityEvent
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import xiaoyiz.InteractionProxies.Identifier.AppId
import xiaoyiz.InteractionProxies.Identifier.Identifier
import xiaoyiz.InteractionProxies.Identifier.WindowTree
import xiaoyiz.InteractionProxies.Identifier.isSameTree
import xiaoyiz.InteractionProxies.Utils.Util
import java.util.ArrayList


class ExperimentService : AccessibilityService() {
    var windowTreeList = ArrayList<WindowTree>()
    var uniqueWindowTreeIdList = ArrayList<String>()
    var duplicatedWindowTreeIdList = ArrayList<Pair<String, String>>()

    override fun onCreate() {
        super.onCreate()

        //val versionCode = Util.getVersionCode(this, packageName)

        val self = this
        val ref = FirebaseDatabase.getInstance().reference.child(Util.AppIdDB).child("com-yelp-android").child("19016503")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Log.i("Xiaoyi", "AppId Not Found.")
                } else {
                    for (activityName in dataSnapshot.child("appActivityList").children) {
                        for (id in activityName.children) {
                            val tree = WindowTree(id.key)
                            self.windowTreeList.add(tree)
                        }
                    }
                }
            }

            override fun onCancelled(firebaseError: DatabaseError) {}
        })

        android.os.Handler().postDelayed({
            var sortedList = self.windowTreeList.sortedWith(compareBy({ it.id }))
            for (i in 0..sortedList.size - 1) {
                for (j in 0 until i) {
                    if (isSameTree(sortedList[i], sortedList[j])) {
                        duplicatedWindowTreeIdList.add(Pair(sortedList[j].id, sortedList[i].id))
                    }
                }
                uniqueWindowTreeIdList.add(sortedList[i].id)
            }

            for (id in uniqueWindowTreeIdList) {
                Log.i("Xiaoyi", id)
            }
            for (pair in duplicatedWindowTreeIdList) {
                Log.i("Xiaoyi", pair.first + ":" + pair.second)
            }
        }, 3000)
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent) {
    }

    override fun onInterrupt() {}
}
