package xiaoyiz.InteractionProxies.Identifier

import android.content.Context
import android.net.Uri
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat
import android.util.Log
import android.view.accessibility.AccessibilityNodeInfo

import com.google.android.apps.common.testing.accessibility.framework.AccessibilityCheckResult
import com.google.android.apps.common.testing.accessibility.framework.AccessibilityCheckResultUtils
import com.google.android.apps.common.testing.accessibility.framework.AccessibilityInfoHierarchyCheck
import com.google.android.apps.common.testing.accessibility.framework.AccessibilityCheckPreset
import com.google.android.apps.common.testing.accessibility.framework.AccessibilityInfoCheckResult
import com.google.android.apps.common.testing.accessibility.framework.SpeakableTextPresentInfoCheck

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import xiaoyiz.InteractionProxies.TalkbackUtils.NodeSpeechRuleProcessor

import java.util.ArrayList
import java.util.Date
import java.util.LinkedList

import xiaoyiz.InteractionProxies.Utils.*


class WindowTree {
    var id = ""
    var packageName = ""
    var activityName = ""
    var viewIdResourceNameList = ArrayList<String>()
    var unlabeledWindowTreeNodeIdList = ArrayList<String>()
    var talkbackWindowTreeNodeIdList = ArrayList<String>()
    var windowTreeNodeList = ArrayList<WindowTreeNode>()
    var addedLabelList = ArrayList<Pair<String, String>>()

    // Download from Firebase
    constructor(id: String) {
        this.id = id

        val self = this
        val ref = FirebaseDatabase.getInstance().reference.child(Util.WindowTreeDB).child(id)
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                self.packageName = dataSnapshot.child("packageName").value as String
                self.activityName = dataSnapshot.child("activityName").value as String

                self.viewIdResourceNameList = dataSnapshot.child("viewIdResourceNameList").getValue(object : GenericTypeIndicator<ArrayList<String>>() {})

                if (dataSnapshot.child("unlabeledWindowTreeNodeIdList").value != null) {
                    self.unlabeledWindowTreeNodeIdList = dataSnapshot.child("unlabeledWindowTreeNodeIdList").getValue(object : GenericTypeIndicator<ArrayList<String>>() {})
                }

                val windowTreeNodeIdList = dataSnapshot.child("windowTreeNodeIdList").getValue(object : GenericTypeIndicator<ArrayList<String>>() {})
                for (id in windowTreeNodeIdList) {
                    val node = WindowTreeNode(self.id, id)
                    self.windowTreeNodeList.add(node)
                }

                ref.child("addedLabelList").addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        for (addedLabel in dataSnapshot.children) {
                            self.addedLabelList.add(
                                    Pair(addedLabel.key,
                                         addedLabel.child("newAltText").value as String))
                        }
                    }

                    override fun onCancelled(p0: DatabaseError?) { }
                })
            }

            override fun onCancelled(firebaseError: DatabaseError) { }
        })
    }

    // Create from client
    constructor(rootAccessibilityNode: AccessibilityNodeInfo,
                activity: String?,
                context: Context) {
        id = Date().time.toString()

        if (rootAccessibilityNode.packageName != null) {
            packageName = rootAccessibilityNode.packageName.toString()
        }
        if (activity != null) {
            activityName = activity
        }

        viewIdResourceNameList = Util.getAllViewIdResourceName(rootAccessibilityNode)

        createWindowTreeNodeList(windowTreeNodeList, rootAccessibilityNode, 0, -1)
        createUnlabeledWindowTreeNodeList(rootAccessibilityNode, context)
        createTalkbackWindowTreeNodeIdList(rootAccessibilityNode, context)

        updateAnchorNode()
    }

    private fun updateAnchorNode() {
        for (node in windowTreeNodeList) {
            if (node.className == "android.widget.TextView" && node.text == null && node.contentDescription == null && node.viewIdResName == "com.yelp.android:id/war_stars_view") {
                node.anchorNodeId = "?"
            }
        }
    }

    private fun createWindowTreeNodeList(windowTreeNodeList: ArrayList<WindowTreeNode>,
                                         accessibilityNode: AccessibilityNodeInfo?,
                                         depth: Int,
                                         parentIndex: Int): Int {
        if (accessibilityNode == null) return -1

        val windowTreeNode = WindowTreeNode(accessibilityNode, depth)
        windowTreeNodeList.add(windowTreeNode)
        val index = windowTreeNodeList.size - 1
        val childrenIndexList = ArrayList<Int>()
        for (i in 0..accessibilityNode.childCount - 1) {
            childrenIndexList.add(createWindowTreeNodeList(windowTreeNodeList, accessibilityNode.getChild(i), depth + 1, index))
        }
        windowTreeNode.parentIndex = parentIndex
        windowTreeNode.childrenIndexList = childrenIndexList
        return index
    }

    private fun createUnlabeledWindowTreeNodeList(rootAccessibilityNode: AccessibilityNodeInfo, context: Context) {
        val checks = AccessibilityCheckPreset.getInfoChecksForPreset(AccessibilityCheckPreset.LATEST)

        val results = LinkedList<AccessibilityInfoCheckResult>()
        for (check in checks) {
            if (check is SpeakableTextPresentInfoCheck) {
                results.addAll(check.runCheckOnInfoHierarchy(rootAccessibilityNode, context))
            }
        }

        val errors = AccessibilityCheckResultUtils.getResultsForType(results, AccessibilityCheckResult.AccessibilityCheckResultType.ERROR)

        for (error in errors) {
            val errorNode = error.info
            for (windowTreeNode in windowTreeNodeList) {
                if (errorNode == windowTreeNode.accessibilityNode) {
                    unlabeledWindowTreeNodeIdList.add(windowTreeNode.id)
                }
            }
        }
    }

    private fun createTalkbackWindowTreeNodeIdList(rootAccessibilityNode: AccessibilityNodeInfo, context: Context) {
        val talkbackOrderNodeList = getNodesInTalkBackOrder(AccessibilityNodeInfoCompat(rootAccessibilityNode))
        for (node in talkbackOrderNodeList) {
            for (windowTreeNode in windowTreeNodeList) {
                if (node == AccessibilityNodeInfoCompat(windowTreeNode.accessibilityNode)) {
                    //Log.i("Xiaoyi", "Node: " + node.toString())
                    talkbackWindowTreeNodeIdList.add(windowTreeNode.id)

                    var talkbackText = NodeSpeechRuleProcessor.getDescriptionForTree(context, node,null, node)
                    windowTreeNode.talkbackText = talkbackText.toString()
                }
            }
        }
    }

    fun writeToFirebase(packageName: String) {
        val ref = FirebaseDatabase.getInstance().reference.child(Util.WindowTreeDB).child(this.id)
        ref.child("packageName").setValue(packageName)
        ref.child("activityName").setValue(activityName)

        ref.child("viewIdResourceNameList").setValue(viewIdResourceNameList)
        ref.child("unlabeledWindowTreeNodeIdList").setValue(unlabeledWindowTreeNodeIdList)
        ref.child("talkbackWindowTreeNodeIdList").setValue(talkbackWindowTreeNodeIdList)

        val list = ArrayList<String>()
        for (node in windowTreeNodeList) {
            list.add(node.id)
            node.writeToFirebase(this.id)
        }
        ref.child("windowTreeNodeIdList").setValue(list)

        val screenshot = Util.getScreenShot()
        if (screenshot != null) {
            val storageRef = FirebaseStorage.getInstance().getReference(id + ".png")
            val uploadTask = storageRef.putBytes(screenshot)
            uploadTask.addOnSuccessListener { taskSnapshot ->
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                val downloadUrl = taskSnapshot.downloadUrl
                Log.i("Xiaoyi", "" + downloadUrl!!)
            }
            uploadTask.addOnFailureListener {
                // Handle unsuccessful uploads
            }
        }
    }

    private fun printTree() {
        for (node in windowTreeNodeList) {
            node.printData()
        }
    }
}
