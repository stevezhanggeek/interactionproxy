package xiaoyiz.InteractionProxies.Utils


import android.content.Context
import android.graphics.Rect
import android.util.Log
import android.view.View
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityManager
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.Button
import xiaoyiz.InteractionProxies.Identifier.WindowTree

import java.util.ArrayList

import xiaoyiz.InteractionProxies.Identifier.WindowTreeNode
import xiaoyiz.InteractionProxies.Utils.*

class LabelHelper {
    var inSwipeNavigationMode = true
    var shouldReturnToAnchor = false

    var anchorNode: AccessibilityNodeInfo? = null
    var focusedNode: AccessibilityNodeInfo? = null
    var clientWindowTree: WindowTree? = null
    var remoteWindowTree: WindowTree? = null
    var overlayList = ArrayList<Overlay>()


    fun removeAllOverlays(context: Context) {
        if (overlayList.size == 0) return

        for (overlay in overlayList) {
            Util.windowManager.removeView(overlay.overlay)
        }
        overlayList = ArrayList<Overlay>()
    }
}


class labelObj(id: String) {
    var label: String? = null

    var node: WindowTreeNode? = null

    var xPath: String? = null
    var className: String? = null
    var viewIdResName: String? = null
    var text: String? = null
    var contentDescription: String? = null
    var depth: Int = 0
    var isLeaf: Boolean = false
}

fun getNodeOnScreen(clientWindowTree: WindowTree, remoteWindowTreeNode: WindowTreeNode) : WindowTreeNode? {
    val perfectMatchingList = ArrayList<WindowTreeNode>()
    val matchingList = ArrayList<WindowTreeNode>()

    for (node in clientWindowTree.windowTreeNodeList) {
        if (node.className != remoteWindowTreeNode.className) { continue }
        if (node.viewIdResName != remoteWindowTreeNode.viewIdResName) { continue }
        if (node.text != remoteWindowTreeNode.text) { continue }
        if (node.contentDescription != remoteWindowTreeNode.contentDescription) { continue }

        perfectMatchingList.add(node)
    }

    if (perfectMatchingList.size > 0) {
        return perfectMatchingList[0]
    } else if (matchingList.size > 0) {
        return matchingList[0]
    } else {
        return null
    }
}


fun shouldIgnoreEvent(event: AccessibilityEvent): Boolean {
    if (event.packageName == null) {
        return true
    }
    if (event.packageName.toString().contains("com.google.android")
            || event.packageName.toString().contains("com.android")
            || event.packageName.toString() == "android") {
        return true
    }
    return false
}

fun sendEmptyAnnouncement(context: Context) {
    val manager = context.getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager
    if (manager.isEnabled) {
        val e = AccessibilityEvent.obtain()
        e.eventType = AccessibilityEvent.TYPE_ANNOUNCEMENT
        e.text.add("")
        manager.sendAccessibilityEvent(e)
    }

}