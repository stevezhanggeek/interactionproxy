(function($) {
  //var windowTreeId = 1499726156943;
  let searchParams = new URLSearchParams(window.location.search);
  var windowTreeId = searchParams.get('windowTreeId');

  var setupScreenshot = function() {
    var imageRef = firebase.storage().ref(windowTreeId+'.png');
    imageRef.getDownloadURL().then(function(url) {
      var img = document.getElementById('screenshot');
      img.src = url;
    }).catch(function(error) {
      // Handle any errors
    });
  }

  var setupMetadata = function() {
    var windowTreeRef = firebase.database().ref("WindowTreeDB").child(windowTreeId);
    windowTreeRef.once('value').then(function(snapshot) {
      document.getElementById("packageName").innerHTML = snapshot.val().packageName;
      document.getElementById("activityName").innerHTML = snapshot.val().activityName;
      document.getElementById("windowId").innerHTML = windowTreeId;

      var img = document.getElementById('screenshot');
      //console.log(img.getBoundingClientRect());
      var zoomRatio = img.width / 1440;
      snapshot.val().windowTreeNodeIdList.forEach(function(nodeId) {
        var windowTreeNodeRef = firebase.database().ref("WindowTreeNodeDB").child(windowTreeId).child(nodeId);
        windowTreeNodeRef.once('value').then(function(snapshot) {
          $('#screenshot_container').append("<div class='nodeElement' id='" + nodeId + "'></div>");
          var boundsInScreen = snapshot.val().boundsInScreen;
          var styles = {
            width: (boundsInScreen.right - boundsInScreen.left)*zoomRatio,
            height: (boundsInScreen.bottom - boundsInScreen.top)*zoomRatio,
            left: boundsInScreen.left*zoomRatio,
            top: boundsInScreen.top*zoomRatio,
            opacity: 0.3,
            position: "absolute",
            zIndex: snapshot.val().depth
          };
          $('#'+nodeId).css(styles);

          $('#'+nodeId).click(function() {
            if (!$(this).hasClass("selectedElement")) {
              $('#nodeViewIdResName').html(snapshot.val().viewIdResName);
              $('#nodeClassName').html(snapshot.val().className);
              $('#nodeText').html(snapshot.val().text);
              $('#nodeContentDescription').html(snapshot.val().contentDescription);

              $('#currentNodeId').html(snapshot.key);

              $('.hoveredElement').removeClass("hoveredElement");
              $('.selectedElement').removeClass("selectedElement");
              $(this).addClass("selectedElement");
              $('#enterNewAltText').css("opacity", 1);
            } else {
              $('.selectedElement').removeClass("selectedElement");
              $('#currentNodeId').html("");
              $('#enterNewAltText').css("opacity", 0);
            }
          });

          $('#'+nodeId).hover(function(){
            if ($('.selectedElement').size() == 0) {
              $(this).addClass("hoveredElement");
              $('#currentNodeId').html(snapshot.key);
              $('#nodeViewIdResName').html(snapshot.val().viewIdResName);
              $('#nodeClassName').html(snapshot.val().className);
              $('#nodeText').html(snapshot.val().text);
              $('#nodeContentDescription').html(snapshot.val().contentDescription);
              $('#nodeAnchor').html(snapshot.val().anchorNodeId);
            }
          }, function(){
            if ($('.selectedElement').size() == 0) {
              $(this).removeClass("hoveredElement");
              $('#currentNodeId').html("");
              $('#nodeViewIdResName').html("");
              $('#nodeClassName').html("");
              $('#nodeText').html("");
              $('#nodeContentDescription').html("");
              $('#nodeAnchor').html("");
            }
          });
        });
      });
    });
  }

  $('#submitButton').click(function() {
    var newAltText = $('#newAltText').val();
    var currentNodeId = $('#currentNodeId').html();

    // Upload to Firebase
    var windowTreeRef = firebase.database().ref("WindowTreeDB").child(windowTreeId);
    windowTreeRef.child("addedLabelList").child(currentNodeId).set({
      newAltText: newAltText,
      creatorId: "Steve"
    });

    var table = $("#task_table_body");
    table.append("<tr id='table_"+ currentNodeId +"''><td>"+newAltText+"</td></tr>")
    $('#table_'+currentNodeId).hover(function(){
      $('#'+currentNodeId).addClass("hoveredElement");
    }, function(){
      $('.hoveredElement').removeClass("hoveredElement");
    });

    $('#newAltText').val("");
    $('.hoveredElement').removeClass("hoveredElement");
    $('.selectedElement').removeClass("selectedElement");
  });

  setupScreenshot();
  setupMetadata();
  
  $( window ).resize(function() {
    // Remove all nodes before re-create nodes
    $('.nodeElement').remove();
    setupMetadata();
  });

})(jQuery);