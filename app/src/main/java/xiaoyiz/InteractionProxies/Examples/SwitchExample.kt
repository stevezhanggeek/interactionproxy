package xiaoyiz.InteractionProxies.Examples

import android.accessibilityservice.AccessibilityService
import android.graphics.Rect
import android.util.Log
import android.view.KeyEvent
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo

import xiaoyiz.InteractionProxies.Identifier.WindowTree

import xiaoyiz.InteractionProxies.Utils.*


class SwitchExample: AccessibilityService() {
    private var labelHelper = LabelHelper()
    private val windowTreeId = "1500851371590"

    override fun onCreate() {
        super.onCreate()

        Util.setup(this)
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent) {
        if (shouldIgnoreEvent(event)) {
            return
        }
        Log.i("Xiaoyi", event.toString())

        val context = this

        if (event.packageName == Util.proxyPackageName) {
            return
        }

        when (event.eventType) {
            AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED -> {
            }

            AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED -> {
                labelHelper.removeAllOverlays(context)

                val activityName = Util.getActivityName(context, event)
                labelHelper = LabelHelper()
                labelHelper.remoteWindowTree = WindowTree(windowTreeId)

                android.os.Handler().postDelayed(
                        {
                            if (rootInActiveWindow != null) {
                                labelHelper.clientWindowTree = WindowTree(rootInActiveWindow, activityName, context)
                                val addedLabelList = labelHelper.remoteWindowTree!!.addedLabelList
                                for (addedLabel in addedLabelList) {
                                    for (remoteNode in labelHelper.remoteWindowTree!!.windowTreeNodeList) {
                                        if (remoteNode.id == addedLabel.first) {
                                            var node = getNodeOnScreen(labelHelper.clientWindowTree!!, remoteNode)
                                            if (node != null) {
                                                YelpStar(context, node.accessibilityNode!!)
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        2000)
            }
        }
    }


    override fun onKeyEvent(event: KeyEvent): Boolean {
        return true
    }


    override fun onInterrupt() {}
}