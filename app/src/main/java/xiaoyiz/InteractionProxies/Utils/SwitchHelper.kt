package xiaoyiz.InteractionProxies.Utils


import android.content.Context
import android.graphics.Rect
import android.util.Log
import android.view.View
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityManager
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.Button
import xiaoyiz.InteractionProxies.Identifier.WindowTree

import java.util.ArrayList

import xiaoyiz.InteractionProxies.Identifier.WindowTreeNode
import xiaoyiz.InteractionProxies.Utils.*

class SwitchHelper {
    var clientWindowTree: WindowTree? = null
    var remoteWindowTree: WindowTree? = null
    var yelpStarList = ArrayList<YelpStar>()


    fun removeAllOverlays(context: Context) {
        if (yelpStarList.size == 0) return

        for (overlay in yelpStarList) {
            Util.windowManager.removeView(overlay.overlay)
        }
        yelpStarList = ArrayList<YelpStar>()
    }
}