package xiaoyiz.InteractionProxies;
import android.accessibilityservice.AccessibilityService;
import android.util.Log;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;


/**
 * Accessibility Service test
 */
public class WindowIdService2 extends AccessibilityService {

    @Override
    public void onCreate() {
        Log.v("THEIA", "Service Created");
    }

    @Override
    protected boolean onGesture(int gestureId) {
        Log.v("THEIA", String.format("onGesture: [type] %s", gIdToString(gestureId)));
        return false;
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {

        Log.v("THEIA", String.format(
                "onAccessibilityEvent: [type] %s [class] %s [package] %s [time] %s",
                idToText(event), event.getClassName(), event.getPackageName(), event.getEventTime()));

        if(idToText(event)=="TYPE_VIEW_HOVER_ENTER" || idToText(event)=="TYPE_VIEW_HOVER_EXIT") {
            Log.v("THEIA", String.format("onHoverEvent: [scrollX] %s [scrollY] %s",
                    event.getScrollX(), event.getScrollY()));

        }
    }

    @Override
    public void onInterrupt() {
        Log.v("THEIA", "INTERRUPTED");
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.v("THEIA", "AccessibilityService allowed");
    }

    /**
     * Converts an ID returned by AccessibilityEvent.getEventType()
     * to a representative String
     */
    private String idToText(AccessibilityEvent event) {
        switch (event.getEventType()) {
            case AccessibilityEvent.TYPE_TOUCH_EXPLORATION_GESTURE_START:
                return "TYPE_TOUCH_EXPLORATION_GESTURE_START";
            case AccessibilityEvent.TYPE_TOUCH_EXPLORATION_GESTURE_END:
                return "TYPE_TOUCH_EXPLORATION_GESTURE_END";
            case AccessibilityEvent.TYPE_TOUCH_INTERACTION_START:
                return "TYPE_TOUCH_INTERACTION_START";
            case AccessibilityEvent.TYPE_TOUCH_INTERACTION_END:
                return "TYPE_TOUCH_INTERACTION_END";
            case AccessibilityEvent.TYPE_GESTURE_DETECTION_START:
                return "TYPE_GESTURE_DETECTION_START";
            case AccessibilityEvent.TYPE_GESTURE_DETECTION_END:
                return "TYPE_GESTURE_DETECTION_END";
            case AccessibilityEvent.TYPE_VIEW_HOVER_ENTER:
                return "TYPE_VIEW_HOVER_ENTER";
            case AccessibilityEvent.TYPE_VIEW_HOVER_EXIT:
                return "TYPE_VIEW_HOVER_EXIT";
            case AccessibilityEvent.TYPE_VIEW_SCROLLED:
                return "TYPE_VIEW_SCROLLED";
            case AccessibilityEvent.TYPE_VIEW_CLICKED:
                return "TYPE_VIEW_CLICKED";
            case AccessibilityEvent.TYPE_VIEW_LONG_CLICKED:
                return "TYPE_VIEW_LONG_CLICKED";
            case AccessibilityEvent.TYPE_VIEW_FOCUSED:
                return "TYPE_VIEW_FOCUSED";
            case AccessibilityEvent.TYPE_VIEW_SELECTED:
                return "TYPE_VIEW_SELECTED";
            case AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED:
                return "TYPE_VIEW_ACCESSIBILITY_FOCUSED";
            case AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED:
                return "TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED";
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                return "TYPE_WINDOW_STATE_CHANGED";
            case AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED:
                return "TYPE_NOTIFICATION_STATE_CHANGED";
            case AccessibilityEvent.TYPE_ANNOUNCEMENT:
                return "TYPE_ANNOUNCEMENT";
            case AccessibilityEvent.TYPE_WINDOWS_CHANGED:
                return "TYPE_WINDOWS_CHANGED";
            case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                return "TYPE_WINDOW_CONTENT_CHANGED";
            case AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED:
                return "TYPE_VIEW_TEXT_CHANGED";
            case AccessibilityEvent.TYPE_VIEW_TEXT_SELECTION_CHANGED:
                return "TYPE_VIEW_TEXT_SELECTION_CHANGED";
            case AccessibilityEvent.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY:
                return "TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY";
        }
        return "Unknown";
    }


    /**
     * Converts gestureID to a representative String
     * @param gID
     * @return
     */
    private String gIdToString(int gID) {
        switch(gID) {
            case 1: return "GESTURE_SWIPE_UP";
            case 2: return "GESTURE_SWIPE_DOWN";
            case 3: return "GESTURE_SWIPE_LEFT";
            case 4: return "GESTURE_SWIPE_RIGHT";
            case 5: return "GESTURE_SWIPE_LEFT_AND_RIGHT";
            case 6: return "GESTURE_SWIPE_RIGHT_AND_LEFT";
            case 7: return "GESTURE_SWIPE_UP_AND_DOWN";
            case 8: return "GESTURE_SWIPE_DOWN_AND_UP";
            case 9: return "GESTURE_SWIPE_LEFT_AND_UP";
            case 10: return "GESTURE_SWIPE_LEFT_AND_DOWN";
            case 11: return "GESTURE_SWIPE_RIGHT_AND_UP";
            case 12: return "GESTURE_SWIPE_RIGHT_AND_DOWN";
            case 13: return "GESTURE_SWIPE_UP_AND_LEFT";
            case 14: return "GESTURE_SWIPE_UP_AND_RIGHT";
            case 15: return "GESTURE_SWIPE_DOWN_AND_LEFT";
            case 16: return "GESTURE_SWIPE_DOWN_AND_RIGHT";
        }
        return "UNKNOWN";
    }

}