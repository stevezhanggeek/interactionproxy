package xiaoyiz.InteractionProxies.Identifier

import android.util.Log
import java.util.*


fun isSameTree(newTree: WindowTree, oldTree: WindowTree): Boolean {
    // Check activity name (caution: activityName may have delay)
    if (newTree.activityName != "" && oldTree.activityName != "" && newTree.activityName != oldTree.activityName) {
        return false
    }

    // Check viewIdResourceName list
    // A high similarity guarantees two trees are same
    // Otherwise we need to check structure
    val viewIdResourceNameSet1 = HashSet(newTree.viewIdResourceNameList)
    val viewIdResourceNameSet2 = HashSet(oldTree.viewIdResourceNameList)

    val common = intersectionsCount(viewIdResourceNameSet1, viewIdResourceNameSet2)
    val union = viewIdResourceNameSet1.size + viewIdResourceNameSet2.size - common
    val similarity = common.toDouble() / union
    Log.i("Xiaoyi", "similarity: " + similarity.toString())

    if (union > 3 && similarity != 1.0) {
        return false
    }
    return true
/*
        // Check nodes
        var sameNodeCount = 0.0
        for (node in newTree.windowTreeNodeList) {
            if (findSameNode(node, oldTree.windowTreeNodeList)) {
                sameNodeCount++
            }
        }
        val nodeSimilarity = sameNodeCount / newTree.windowTreeNodeList.size
        Log.i("Xiaoyi", "nodeSimilarity: " + nodeSimilarity.toString())

        if (nodeSimilarity > 0.95) {
            return true
        }
*/
    /*

// Check nodes hierarchy
double hierarchyCount = calculateHierarchySimilarity(newTree.windowTreeNodeList.get(0), newTree,
        oldTree.windowTreeNodeList.get(0), oldTree);
double hierarchySimilarity = hierarchyCount / Math.min(newTree.windowTreeNodeList.size(), oldTree.windowTreeNodeList.size());
Log.i("Xiaoyi", "hierarchySimilarity: " + String.valueOf(hierarchySimilarity));

if (hierarchySimilarity > 0.95) { return true; }
    // Check nodes hierarchy with wildcard
    val result = matchTwoTrees(newTree, oldTree)
    Log.i("Xiaoyi", "Match: " + result)
    return result
*/
}

fun intersectionsCount(set1: Set<*>, set2: Set<*>): Int {
    if (set2.size < set1.size) return intersectionsCount(set2, set1)
    var count = 0
    for (o in set1) {
        if (set2.contains(o)) {
            count++
        }
    }
    return count
}

fun findSameNode(node: WindowTreeNode, nodeList: ArrayList<WindowTreeNode>): Boolean {
    for (nodeInList in nodeList) {
        if (nodeInList.depth == node.depth
                && nodeInList.className == node.className
                && nodeInList.text == node.text
                && nodeInList.contentDescription == node.contentDescription
                && nodeInList.viewIdResName == node.viewIdResName
                && nodeInList.contentDescription == node.contentDescription) {
            return true
        }
    }
    return false
}

private fun calculateHierarchySimilarity(node1: WindowTreeNode, tree1: WindowTree,
                                         node2: WindowTreeNode, tree2: WindowTree): Int {
    if (node1.className != null && node1.className == node2.className) {
        var sum = 1
        for (i in 0..Math.min(node1.childrenIndexList!!.size, node2.childrenIndexList!!.size) - 1) {
            sum += calculateHierarchySimilarity(tree1.windowTreeNodeList[node1.childrenIndexList!![i]], tree1,
                    tree2.windowTreeNodeList[node2.childrenIndexList!![i]], tree2)
        }
        return sum
    } else {
        return 0
    }
}

private fun isSameNode(node1: WindowTreeNode?, node2: WindowTreeNode?): Boolean {
    if (node1 == null || node2 == null) return false

    if (node1.className != node2.className) return false

    if (node1!!.isLeaf != node2!!.isLeaf) return false

    return true
}

private fun matchTwoTrees(tree1: WindowTree, tree2: WindowTree): Boolean {
    if (tree1.windowTreeNodeList[tree1.windowTreeNodeList.size - 1].depth != tree2.windowTreeNodeList[tree2.windowTreeNodeList.size - 1].depth) {
        //return false;
    }
    // Set up
    val queue1 = LinkedList<WindowTreeNode>()
    val queue2 = LinkedList<WindowTreeNode>()
    queue1.add(tree1.windowTreeNodeList[0])
    queue2.add(tree2.windowTreeNodeList[0])
    var mostRecentPopNode1: WindowTreeNode? = null
    var mostRecentPopNode2: WindowTreeNode? = null

    var badCount = 0

    while (queue1.size > 0 && queue2.size > 0) {
        val node1 = queue1.peek()
        val node2 = queue2.peek()
        for (index in node1.childrenIndexList!!) {
            queue1.add(tree1.windowTreeNodeList[index])
        }
        for (index in node2.childrenIndexList!!) {
            queue2.add(tree2.windowTreeNodeList[index])
        }

        if (!isSameNode(node1, node2)) {
            // Neither of node1 and node2 has list siblings.
            if (!isSameNode(mostRecentPopNode1, node1) && !isSameNode(mostRecentPopNode2, node2)) {
                badCount++
                if (node1.depth > node2.depth) {
                    mostRecentPopNode1 = queue1.poll()
                } else {
                    mostRecentPopNode2 = queue2.poll()
                }
                Log.i("Xiaoyi", "NotSameNode" + node1.className + "," + node1.boundsInScreen.toShortString() + "," + node2.className + "," + node2.boundsInScreen.toShortString())

                if (badCount == 3) {
                    return false
                }
            } else {
                if (isSameNode(mostRecentPopNode1, node1)) {
                    mostRecentPopNode1 = queue1.poll()
                } else if (isSameNode(mostRecentPopNode2, node2)) {
                    mostRecentPopNode2 = queue2.poll()
                }
            }

        } else {
            // Pop first node
            mostRecentPopNode1 = queue1.poll()
            mostRecentPopNode2 = queue2.poll()
        }
    }

    return true
}