package xiaoyiz.InteractionProxies;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class ProxyTest extends LinearLayout {
    public View prev_button;
    public View next_button;
    public Button correct_button;

    private void inflateLayout(Context context) {
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.overlay_buttons, this);

        prev_button = view.findViewById(R.id.overlayPrev);
        next_button = view.findViewById(R.id.overlayNext);
        correct_button = (Button) view.findViewById(R.id.correct_button);
    }
    public ProxyTest(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateLayout(context);
    }

    public ProxyTest(Context context) {
        super(context);
        inflateLayout(context);
    }
}