package xiaoyiz.InteractionProxies.Identifier

import android.content.Context
import android.graphics.Rect
import android.net.Uri
import android.util.Log
import android.view.accessibility.AccessibilityNodeInfo

import com.google.android.apps.common.testing.accessibility.framework.AccessibilityCheckResult
import com.google.android.apps.common.testing.accessibility.framework.AccessibilityCheckResultUtils
import com.google.android.apps.common.testing.accessibility.framework.AccessibilityInfoHierarchyCheck
import com.google.android.apps.common.testing.accessibility.framework.AccessibilityCheckPreset
import com.google.android.apps.common.testing.accessibility.framework.AccessibilityInfoCheckResult
import com.google.android.apps.common.testing.accessibility.framework.SpeakableTextPresentInfoCheck

import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask

import java.util.ArrayList
import java.util.Date
import java.util.HashMap
import java.util.HashSet
import java.util.LinkedList
import java.util.Queue
import java.util.Stack

import xiaoyiz.InteractionProxies.Utils.Util

class AppId {
    var createdAt = "";
    var traceCreatedAt = "";

    // In case empty string will break firebase (most of the case it will be overwrite by new activity, e.g. the start activity; but just in case app switch etc)
    var mostRecentActivityName = "InitEmpty"

    var packageName = ""
    var versionCode = -1
    var screenW = 0
    var screenH = 0
    var appWindowTreeList = ArrayList<WindowTree>()

    val firebaseAppIdRef: DatabaseReference
        get() = FirebaseDatabase.getInstance().reference.child(Util.AppIdDB).child(packageName).child(versionCode.toString())

    constructor(packageName: String, versionCode: Int) {
        this.packageName = Util.formatForFirebase(packageName)
        this.versionCode = versionCode
        this.screenW = Util.screenW
        this.screenH = Util.screenH

        val self = this
        val ref = firebaseAppIdRef
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    // Create new AppId in Firebase
                    ref.child("screenW").setValue(screenW)
                    ref.child("screenH").setValue(screenH)
                    ref.child("createdAt").setValue((System.currentTimeMillis()/1000).toString())
                    Log.i("Xiaoyi", "New AppId created.")
                } else {
                    for (activityName in dataSnapshot.child("appActivityList").children) {
                        for (id in activityName.children) {
                            val tree = WindowTree(id.key)
                            self.appWindowTreeList.add(tree)
                        }
                    }
                }
                self.initNewTrace();
            }

            override fun onCancelled(firebaseError: DatabaseError) {

            }
        })
    }

    fun initNewTrace() {
        traceCreatedAt = (System.currentTimeMillis()/1000).toString()
    }

    fun addWindow(root: AccessibilityNodeInfo?, activityName: String?, context: Context): Boolean {
        if (root == null) return false

        var activity: String? = activityName
        if (activityName == null) {
            activity = mostRecentActivityName
        }

        val newTree = WindowTree(root, activityName, context)

        val ref = firebaseAppIdRef
        val treeCreatedAt = (System.currentTimeMillis()/1000).toString()
        ref.child("traceList").child(traceCreatedAt).child(treeCreatedAt).child("windowTreeNodeId").setValue(newTree.id)

        for (tree in appWindowTreeList) {
            if (isSameTree(newTree, tree)) {
                Log.i("Xiaoyi", "Old Tree ID: " + tree.id)
                newTree.writeToFirebase(packageName)
                ref.child("appActivityList").child(Util.formatForFirebase(activity)).child(tree.id).child(newTree.id).setValue(true)
                ref.child("traceList").child(traceCreatedAt).child(treeCreatedAt).child("oldTreeNodeId").setValue(tree.id)
                return false
            }
        }

        // No same tree found
        appWindowTreeList.add(newTree)
        Log.i("Xiaoyi", "New Tree ID: " + newTree.id)
        newTree.writeToFirebase(packageName)
        ref.child("appActivityList").child(Util.formatForFirebase(activity)).child(newTree.id).setValue(true)
        ref.child("traceList").child(traceCreatedAt).child(treeCreatedAt).child("oldTreeNodeId").setValue("")

        return true
    }
}
