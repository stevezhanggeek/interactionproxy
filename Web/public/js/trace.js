(function($) {
  var packageNameList = [];
  var versionCodeList = [];
  var traceIdList = [];

  var uniqueWindowTreeIdList = [];
  var duplicateWindowTreeIdList = [];
  var updateWindowTreeIdList = [];
  var removeWindowTreeIdList = [];

  var traceRef;

  var addAllBadge = function() {
    updateWindowTreeIdList.forEach(function(updateWindowTreeId){
      $("#"+updateWindowTreeId).find(".label-primary").css("opacity", 1);
    });
    removeWindowTreeIdList.forEach(function(removeWindowTreeId){
      $("#"+removeWindowTreeId).find(".label-danger").css("opacity", 1);
    });
  }

  var enableClickCopy = function() {
    $(".card").click(function() {
      console.log("X");
      var aux = document.createElement("input");
      aux.setAttribute("value", $(this).find(".card-text").html());
      document.body.appendChild(aux);
      aux.select();
      document.execCommand("copy");
      document.body.removeChild(aux);
    });
  }

  var resetCorrectionParams = function() {
    $("#screenshotGrid").empty();
    $("#uniqueId").val("");
    $("#duplicateId").val("");
    $("#wrongId").val("");
    uniqueWindowTreeIdList = [];
    duplicateWindowTreeIdList = [];
    updateWindowTreeIdList = [];
    removeWindowTreeIdList = [];  
  }

  var refreshTrace = function() {
    resetCorrectionParams();
    showTrace($("#packageName").val(), $("#versionCode").val(), $("#traceId").val());
  }

  var addUniqueScreenshot = function(windowTreeId) {
    var templateCard = $('#templateCard').clone();
    templateCard.find(".card").attr('id', windowTreeId);
    templateCard.find(".card-text").text(windowTreeId);

    $("#screenshotGrid").append(templateCard.html());

    var imageRef = firebase.storage().ref(windowTreeId+'.png');
    imageRef.getDownloadURL().then(function(url) {
      $("#"+windowTreeId).find(".card-img-top").attr('src', url);
    }).catch(function(error) {
    });
  }

  var addDuplicateScreenshot = function(obj) {
    var templateCard = $('#templateCardAppended').clone();
    templateCard.find(".card").attr('id', obj.windowTreeId);
    templateCard.find(".card-text").text(obj.windowTreeId);

    $("#"+obj.oldTreeId).parent().append(templateCard.html());

    var imageRef = firebase.storage().ref(obj.windowTreeId+'.png');
    imageRef.getDownloadURL().then(function(url) {
      $("#"+obj.windowTreeId).find(".card-img-top").attr('src', url);
    }).catch(function(error) {
    });
  }

  var showResult = function() {
    uniqueWindowTreeIdList.sort();
    uniqueWindowTreeIdList.forEach(function(windowTreeId) {
      addUniqueScreenshot(windowTreeId);
    });

    duplicateWindowTreeIdList.forEach(function(obj) {
      addDuplicateScreenshot(obj);
    });    
    addAllBadge();
    enableClickCopy();
  }

  var showTrace = function(packageName, versionCode, traceId) {
    traceRef = firebase.database().ref("AppIdDB_Test").child(packageName).child(versionCode).child("traceList").child(traceId)
    traceRef.once('value').then(function(trace){
      trace.forEach(function(tree) {
        var oldTreeId = tree.val().oldTreeNodeId;
        if (oldTreeId == "") {
          uniqueWindowTreeIdList.push(tree.val().windowTreeNodeId);
        } else {
          duplicateWindowTreeIdList.push({
            oldTreeId: oldTreeId,
            windowTreeId: tree.val().windowTreeNodeId
          });
        }

        if (tree.val().correctionDone == "Update") {
          updateWindowTreeIdList.push(tree.val().windowTreeNodeId)
        }
        if (tree.val().correctionDone == "Remove") {
          removeWindowTreeIdList.push(tree.val().windowTreeNodeId)
        }
      });
    });
    $("#correctionForm").css("opacity", 1);
    setTimeout(showResult, 1000);
  }

  $('#updateButton').click(function() {
    var uniqueId = $('#uniqueId').val();
    var duplicateId = $('#duplicateId').val();

    // Upload to Firebase
    traceRef.once('value').then(function(trace){
      trace.forEach(function(tree) {
        if (tree.val().windowTreeNodeId == duplicateId) {
          traceRef.child(tree.key).set({
            oldTreeNodeId: uniqueId,
            windowTreeNodeId: duplicateId,
            correctionDone: "Update"
          });
        }
      });
    });
    setTimeout(refreshTrace, 500);
  });


  $('#removeButton').click(function() {
    var wrongId = $('#wrongId').val();

    // Upload to Firebase
    traceRef.once('value').then(function(trace){
      trace.forEach(function(tree) {
        if (tree.val().windowTreeNodeId == wrongId) {
          traceRef.child(tree.key).set({
            oldTreeNodeId: "",
            windowTreeNodeId: wrongId,
            correctionDone: "Remove"
          });
        }
      });
    });
    setTimeout(refreshTrace, 500);
  });

  var fillPackageNameList = function() {
    firebase.database().ref("AppIdDB_Test").once('value').then(function(appIdList){
      appIdList.forEach(function(appId) {
        packageNameList.push({label: appId.key})
      });
    });
  }

  var fillVersionCodeList = function(packageName) {
    versionCodeList = [];
    var versionCodeEmpty = true;
    firebase.database().ref("AppIdDB_Test").child(packageName).once('value').then(function(vList){
      vList.forEach(function(versionCode) {
        if (versionCodeEmpty) {
          $("#versionCode").val(versionCode.key)
          versionCodeEmpty = false;

          fillTraceIdList(packageName, versionCode.key);
        }
        versionCodeList.push({label: versionCode.key})
      });
      versionCodeAutocomplete();
    });
  }

  var fillTraceIdList = function(packageName, versionCode) {
    traceIdList = [];
    var traceIdEmpty = true;
    firebase.database().ref("AppIdDB_Test").child(packageName).child(versionCode).child("traceList").once('value').then(function(traceList){
      traceList.forEach(function(traceId) {
        if (traceIdEmpty) {
          $("#traceId").val(traceId.key)
          traceIdEmpty = false;

          showTrace(packageName, versionCode, traceId.key);
        }
        traceIdList.push({label: traceId.key})
      });
      traceIdAutocomplete();
    });
  }

  // AutoComplete
  $("#packageName").autocomplete({
    source: packageNameList,
    minLength: 0,
    select: function (event, ui) {
      resetCorrectionParams();
      fillVersionCodeList(ui.item.label);
    }
  });
  var versionCodeAutocomplete = function() {
    $("#versionCode").autocomplete({
      source: versionCodeList,
      minLength: 0,
      select: function (event, ui) {
        resetCorrectionParams();
        fillTraceIdList($("#packageName").val(), ui.item.label);
      }
    });
  };
  var traceIdAutocomplete = function() {
    $("#traceId").autocomplete({
      source: traceIdList,
      minLength: 0,
      select: function (event, ui) {
        resetCorrectionParams();
        showTrace($("#packageName").val(), $("#versionCode").val(), ui.item.label);
      }
    });
  }
  $("#packageName").click(function() {
    $("#packageName").autocomplete("search", "");
  });
  $("#versionCode").click(function() {
    $("#versionCode").autocomplete("search", "");
  });
  $("#traceId").click(function() {
    $("#traceId").autocomplete("search", "");
  });

  versionCodeAutocomplete();
  traceIdAutocomplete();

  fillPackageNameList();

  $(window).resize(function() {
    // Remove all nodes before re-create nodes
    //$('#screenshotGrid').empty();
  });

})(jQuery);