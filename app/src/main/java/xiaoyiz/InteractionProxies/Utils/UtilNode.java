package xiaoyiz.InteractionProxies.Utils;

import android.graphics.Rect;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.ArrayList;
import java.util.List;

public class UtilNode {
    static public class Node {
        private AccessibilityNodeInfo accessibilityNode = null;
        private Rect boundsInParent = new Rect();
        private Rect boundsInScreen = new Rect();

        public Node(AccessibilityNodeInfo node) {
            accessibilityNode = node;

        }
    }
}
