package xiaoyiz.InteractionProxies.Capture;

import android.accessibilityservice.AccessibilityService;
import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;

import com.google.firebase.database.FirebaseDatabase;

import xiaoyiz.InteractionProxies.Identifier.AppId;
import xiaoyiz.InteractionProxies.Identifier.Identifier;
import xiaoyiz.InteractionProxies.Utils.Util;


public class ScreenService extends AccessibilityService {
    @Override
    public void onCreate() {
        super.onCreate();
        Util.setup(this);
        //FirebaseDatabase.getInstance().getReference().removeValue();
    }


    private void onScreenChanged(final Context context, final AccessibilityEvent event) {
        final String packageName = event.getPackageName().toString();
        final AppId appId = Identifier.INSTANCE.getAppId(packageName, context);

        final String activityName = Util.getActivityName(context, event);
        if (activityName != null) {
            appId.setMostRecentActivityName(activityName);
            Log.i("Xiaoyi", "Activity: " + activityName);
        }
        Log.i("Xiaoyi", "-----------");

        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                appId.addWindow(getRootInActiveWindow(), activityName, context);
            }
        }, 3000);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (Util.shouldIgnoreEvent(event)) { return; }

        switch (event.getEventType()) {
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                onScreenChanged(this, event);
                break;
        }
    }

    @Override
    protected boolean onKeyEvent(KeyEvent event) {
        return true;
    }


    @Override
    public void onInterrupt() {
    }
}
