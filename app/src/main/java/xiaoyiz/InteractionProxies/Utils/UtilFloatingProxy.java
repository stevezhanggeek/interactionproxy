package xiaoyiz.InteractionProxies.Utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import xiaoyiz.InteractionProxies.ProxyTest;
import xiaoyiz.InteractionProxies.R;

public class UtilFloatingProxy {
    public static class FloatingProxy {
        public AccessibilityNodeInfo nodeToFix;
        public String correctLabel;
        public LinearLayout overlay;
        public Button correctLabelButton;
        public Rect bounds = new Rect();
        public FloatingProxy(Context context,
                             AccessibilityNodeInfo node,
                             String label) {
            nodeToFix = node;
            correctLabel = label;

            overlay = new LinearLayout(context);
            overlay.setOrientation(LinearLayout.VERTICAL);
            overlay.setBackgroundColor(Color.argb(150, 0, 180, 0));
            overlay.setAlpha((float) 0.3);

            nodeToFix.getBoundsInScreen(bounds);

            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    bounds.width(),
                    bounds.height(),
                    bounds.left,
                    bounds.top - Util.statusBarHeight,
                    WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
            params.gravity = Gravity.TOP | Gravity.LEFT;
            Util.windowManager.addView(overlay, params);
            Util.overlayList.add(overlay);

            LinearLayout prev = new LinearLayout(context);
            prev.setFocusable(true);
            //prev.setContentDescription("prev");
            prev.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
            prev.setBackgroundColor(Color.argb(150, 0, 0, 180));
            prev.setPadding(0, 0, 0, 0);
            //prev.setClickable(true);
            //prev.setEnabled(true);
            //prev.setId(View.generateViewId());
            //prev.dispatchPopulateAccessibilityEvent();

            LinearLayout next = new LinearLayout(context);
            next.setFocusable(true);
            next.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2));
            next.setBackgroundColor(Color.argb(150, 180, 0, 0));
            next.setPadding(0, 0, 0, 0);

            correctLabelButton = new Button(context);
            correctLabelButton.setPadding(0, 0, 0, 0);
            correctLabelButton.setText(label);
            //correctLabelButton.setAccessibilityTraversalAfter(prev.getId());
            correctLabelButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, bounds.height() - 3));

            overlay.addView(prev);
            overlay.addView(correctLabelButton);
            overlay.addView(next);

            /*
            overlay = new ListView(context);
            overlay.setContentDescription("");
            overlay.setBackgroundColor(Color.argb(150, 0, 180, 0));
            overlay.setAlpha((float) 0.3);
            overlay.setDivider(null);

            nodeToFix.getBoundsInScreen(bounds);

            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    bounds.width(),
                    bounds.height(),
                    bounds.left,
                    bounds.top - Util.statusBarHeight,
                    WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
            params.gravity = Gravity.TOP | Gravity.LEFT;
            Util.windowManager.addView(overlay, params);
            Util.overlayList.add(overlay);

            String[] list_content = new String[] { "S", "Start", "B" };

            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                    (context, android.R.layout.simple_list_item_1, list_content){
                @Override
                public View getView(int position, View convertView, ViewGroup parent){
                    /// Get the Item from ListView
                    View view = super.getView(position, convertView, parent);
                    TextView tv = (TextView) view.findViewById(android.R.id.text1);
                    tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
                    ViewGroup.LayoutParams params = view.getLayoutParams();
                    params.height = 60;
                    view.setLayoutParams(params);
                    return view;
                }
            };

            //ArrayAdapter<String> list_adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_2, android.R.id.text1, list_content);
            overlay.setAdapter(arrayAdapter);

            overlay.setAccessibilityDelegate(new View.AccessibilityDelegate() {
                @Override
                public void onInitializeAccessibilityNodeInfo(View host, AccessibilityNodeInfo info) {
                    super.onInitializeAccessibilityNodeInfo(host, info);
                    // Do some stuff with info
                }
            });
            */
        }
    }
}
