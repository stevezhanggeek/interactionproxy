package xiaoyiz.InteractionProxies.Identifier

import android.content.Context
import java.util.ArrayList

import xiaoyiz.InteractionProxies.Utils.Util

object Identifier {
    private val appIdList = ArrayList<AppId>()

    fun getAppId(packageName: String, context: Context): AppId {
        val versionCode = Util.getVersionCode(context, packageName)
        for (appId in appIdList) {
            if (appId.packageName == Util.formatForFirebase(packageName) && appId.versionCode == versionCode) {
                return appId
            }
        }
        val newAppId = AppId(packageName, versionCode)
        appIdList.add(newAppId)
        return newAppId
    }
}
