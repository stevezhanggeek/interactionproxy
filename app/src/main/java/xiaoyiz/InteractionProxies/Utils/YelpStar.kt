package xiaoyiz.InteractionProxies.Utils

import android.content.Context
import android.graphics.Color
import android.graphics.PixelFormat
import android.graphics.Rect
import android.view.Gravity
import android.view.WindowManager
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.LinearLayout
import android.accessibilityservice.AccessibilityService
import android.util.Log
import android.view.View
import java.util.ArrayList


class YelpStar {
    var accessibilityNode: AccessibilityNodeInfo
    var overlay: LinearLayout
    var bounds = Rect()

    constructor(context: Context,
                accessibilityNode: AccessibilityNodeInfo) {
        overlay = LinearLayout(context)
        overlay.orientation = LinearLayout.HORIZONTAL
        overlay.setBackgroundColor(Color.argb(30, 255, 0, 0))
        //overlay.alpha = 0.3.toFloat()

        this.accessibilityNode = accessibilityNode

        this.accessibilityNode.getBoundsInScreen(bounds)

        val params = WindowManager.LayoutParams(
                bounds.width(),
                bounds.height(),
                bounds.left,
                bounds.top - Util.statusBarHeight,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT)
        params.gravity = Gravity.TOP or Gravity.LEFT
        Util.windowManager.addView(overlay, params)
        Util.overlayList.add(overlay)

        val stars = ArrayList<View>()
        stars.add(View(context))
        stars.add(View(context))
        stars.add(View(context))
        stars.add(View(context))
        stars.add(View(context))

        for (i in stars.indices) {
            val star = stars.get(i)
            Log.i("Xiaoyi", ""+i)
            star.setLayoutParams(LinearLayout.LayoutParams(bounds.width() / 5, bounds.height()))
            star.setBackgroundColor(Color.argb(i * 10, 0, 255, 0))
            star.setClickable(true)
            star.setOnClickListener({ view ->
                Util.windowManager.removeView(overlay)

                val starLocation = IntArray(2)
                view.getLocationOnScreen(starLocation)
                (context as AccessibilityService).dispatchGesture(Util.gestureOneFingerClick(starLocation[0] + view.width/2, starLocation[1] + view.height/2, 300), null, null)
            })
            overlay.addView(star, i)
        }
    }
}