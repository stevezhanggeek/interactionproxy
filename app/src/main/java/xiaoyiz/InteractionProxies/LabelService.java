package xiaoyiz.InteractionProxies;

import android.accessibilityservice.AccessibilityService;
import android.content.Context;
import android.graphics.Rect;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.ArrayList;

import xiaoyiz.InteractionProxies.Utils.Util;
import xiaoyiz.InteractionProxies.Utils.UtilFloatingProxy;


public class LabelService extends AccessibilityService {
    private String appPackageName = "com.toggl.timer";
    private AccessibilityNodeInfo focusedUnlabeledNode;
    private ArrayList<UtilFloatingProxy.FloatingProxy> proxyList = new ArrayList<>();
    private ArrayList<AccessibilityNodeInfo> unlabeledNodeList = new ArrayList<>();
    private static String currentWindowId = "";

    @Override
    public void onCreate() {
        super.onCreate();

        Util.setup(this);
    }

    private String gIdToString(int gID) {
        switch(gID) {
            case 1: return "GESTURE_SWIPE_UP";
            case 2: return "GESTURE_SWIPE_DOWN";
            case 3: return "GESTURE_SWIPE_LEFT";
            case 4: return "GESTURE_SWIPE_RIGHT";
            case 5: return "GESTURE_SWIPE_LEFT_AND_RIGHT";
            case 6: return "GESTURE_SWIPE_RIGHT_AND_LEFT";
            case 7: return "GESTURE_SWIPE_UP_AND_DOWN";
            case 8: return "GESTURE_SWIPE_DOWN_AND_UP";
            case 9: return "GESTURE_SWIPE_LEFT_AND_UP";
            case 10: return "GESTURE_SWIPE_LEFT_AND_DOWN";
            case 11: return "GESTURE_SWIPE_RIGHT_AND_UP";
            case 12: return "GESTURE_SWIPE_RIGHT_AND_DOWN";
            case 13: return "GESTURE_SWIPE_UP_AND_LEFT";
            case 14: return "GESTURE_SWIPE_UP_AND_RIGHT";
            case 15: return "GESTURE_SWIPE_DOWN_AND_LEFT";
            case 16: return "GESTURE_SWIPE_DOWN_AND_RIGHT";
        }
        return "UNKNOWN";
    }

    @Override
    protected boolean onGesture(int gestureId) {
        Log.v("THEIA", String.format("onGesture: [type] %s", gIdToString(gestureId)));
        return false;
    }

    private void onFocused(AccessibilityEvent event, Context context) {
        // When user swipes to move focus on unlabeled node.
        if (event.getPackageName().equals(appPackageName)) {
            // Proxy move this focus.
            if (focusedUnlabeledNode != null) {
                focusedUnlabeledNode = null;
                return;
            }

            // Find the proxy that above this unlabeled item, switch focus to proxy.
            for (UtilFloatingProxy.FloatingProxy proxy: proxyList) {
                if (proxy.nodeToFix.equals(event.getSource())) {
                    focusedUnlabeledNode = event.getSource();

                    switch (Util.screenReader) {
                        case TALKBACK:
                            proxy.correctLabelButton.performAccessibilityAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS, null);
                            break;
                        case SHINEPLUS:
                            AccessibilityManager manager = (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
                            if (manager.isEnabled()) {
                                AccessibilityEvent e = AccessibilityEvent.obtain();
                                e.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
                                //e.setClassName(getClass().getName());
                                //e.setPackageName(context.getPackageName());
                                e.getText().add("Correct Label");
                                manager.sendAccessibilityEvent(e);
                            }
                            break;
                    }
                }
            }
        }

        // When user swipes while the focus is on proxy.
        if (event.getPackageName().equals(Util.proxyPackageName)) {
            Rect bounds = new Rect();
            event.getSource().getBoundsInScreen(bounds);
            if (bounds.height() == 1) {
                focusedUnlabeledNode.performAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS);
                dispatchGesture(Util.leftSwipe(), null, null);
            }
            if (bounds.height() == 2) {
                focusedUnlabeledNode.performAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS);
                dispatchGesture(Util.rightSwipe(), null, null);
            }
        }
    }

    private void onScreenChanged(final Context context, final AccessibilityEvent event) {
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /*
                String activity = Util.getActivityName(context, event);
                if (activity != null) {
                    Log.i("Xiaoyi", "Activity: " + activity);
                }
                ArrayList<String> viewIdResourceNameList = Util.getAllViewIdResourceName(getRootInActiveWindow());
                for (String viewIdResourceName : viewIdResourceNameList) {
                    Log.i("Xiaoyi", viewIdResourceName);
                }
                Log.i("Xiaoyi", "-----------");
                String activity = Util.getActivityName(context, event);
                String windowId = UtilWindowTree.getWindowId(getRootInActiveWindow(), activity);
                // New window entered
                if (!windowId.equals(currentWindowId)) {
                    Util.removeAllOverlays(context);

                    unlabeledNodeList = Util.getAllUnlabeledNodes(getRootInActiveWindow());
                    for (AccessibilityNodeInfo node : unlabeledNodeList) {
                        if (node.getViewIdResourceName() != null && node.getViewIdResourceName().equals("com.toggl.timer:id/ContinueImageButton")) {
                            proxyList.add(new UtilFloatingProxy.FloatingProxy(context, node, "Start"));
                        }
                    }
                } else {
                    // Do nothing
                }
                // Update
                currentWindowId = windowId;
                */
            }
        }, 500);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (Util.shouldIgnoreEvent(event)) return;

        switch (event.getEventType()) {
            case AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED:
                //onFocused(event, this);
                break;
            case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                String activity = Util.getActivityName(this, event);
                if (activity != null) {
                    Log.i("Xiaoyi", "Activity: " + activity);
                }
                break;
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                onScreenChanged(this, event);
                break;
            case AccessibilityEvent.TYPE_TOUCH_EXPLORATION_GESTURE_START:
                Log.i("Xiaoyi", "TYPE_TOUCH_EXPLORATION_GESTURE_START");
                break;
            case AccessibilityEvent.TYPE_TOUCH_EXPLORATION_GESTURE_END:
                Log.i("Xiaoyi", "TYPE_TOUCH_EXPLORATION_GESTURE_END");
                break;
            case AccessibilityEvent.TYPE_TOUCH_INTERACTION_START:
                Log.i("Xiaoyi", "TYPE_TOUCH_INTERACTION_START");
                break;
            case AccessibilityEvent.TYPE_TOUCH_INTERACTION_END:
                Log.i("Xiaoyi", "TYPE_TOUCH_INTERACTION_END");
                break;
        }
    }


    @Override
    protected boolean onKeyEvent(KeyEvent event) {
        return true;
    }


    @Override
    public void onInterrupt() {
    }
}
