package xiaoyiz.InteractionProxies;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.hardware.display.DisplayManager;
import android.media.ImageReader;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import xiaoyiz.InteractionProxies.Utils.Util;

public class MainActivity extends Activity {

	public static int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE= 5469;

	@TargetApi(23)
	public void testOverlayPermission() {
		if (!Settings.canDrawOverlays(this)) {
			Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
					Uri.parse("package:" + getPackageName()));
			startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);
		}
	}

	@TargetApi(23)
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE) {
			if (Settings.canDrawOverlays(this)) {
				System.out.println("WowICan");
			}
		} else {
            Point size = new Point();
            Display display = getWindowManager().getDefaultDisplay();
            display.getRealSize(size);

            DisplayMetrics displayMetrics = new DisplayMetrics();
            display.getMetrics(displayMetrics);
			Util.screenW = size.x;
			Util.screenH = size.y;

			Util.imageReader = ImageReader.newInstance(Util.screenW, Util.screenH, PixelFormat.RGBA_8888, 2);
            Util.virtualDisplay = Util.mediaProjectionManager.getMediaProjection(Activity.RESULT_OK, data).createVirtualDisplay("capture", Util.screenW, Util.screenH, displayMetrics.densityDpi, DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR, Util.imageReader.getSurface(), null, null);
			Log.i("Xiaoyi", ""+Util.imageReader);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (Build.VERSION.SDK_INT >= 23){
			testOverlayPermission();
		}

        this.findViewById(R.id.activateAccessibilityButton).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
			}
		});

		Util.mediaProjectionManager = (MediaProjectionManager)getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        this.findViewById(R.id.openAppButton).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivityForResult(Util.mediaProjectionManager.createScreenCaptureIntent(), 1);
			}
		});

	}

    @Override
    protected void onResume() {
		super.onResume();
	}
}
