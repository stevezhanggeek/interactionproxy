package xiaoyiz.InteractionProxies.Capture

import android.accessibilityservice.AccessibilityService
import android.content.Context
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.accessibility.AccessibilityEvent

import com.google.firebase.database.FirebaseDatabase

import xiaoyiz.InteractionProxies.Identifier.Identifier
import xiaoyiz.InteractionProxies.Identifier.AppId
import xiaoyiz.InteractionProxies.Utils.*
import android.media.RingtoneManager


class CaptureService : AccessibilityService() {
    private var runnable = Runnable {}
    private var handler = android.os.Handler()
    private var contentChangedCount = 0

    private var appId: AppId? = null
    private var activityName: String? = null

    override fun onCreate() {
        super.onCreate()
        Util.setup(this)

        if (appId != null) {
            appId!!.initNewTrace();
        }

        //FirebaseDatabase.getInstance().reference.removeValue()
    }


    private fun onScreenChanged(context: Context, event: AccessibilityEvent) {
        val packageName = event.packageName.toString()
        appId = Identifier.getAppId(packageName, context)

        updateActivityName(context, event)
    }

    private fun onContentChanged(context: Context, event: AccessibilityEvent) {
        updateActivityName(context, event)
    }

    private fun updateActivityName(context: Context, event: AccessibilityEvent) {
        activityName = Util.getActivityName(context, event)
        if (activityName != null) {
            appId?.mostRecentActivityName = activityName!!
        }
    }

    private fun captureScreen(context: Context) {
        Log.i("Xiaoyi", "captureScreen")
        appId?.addWindow(rootInActiveWindow, activityName, context)
        // Sound will show the control widget...not a good idea.
        //val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        //val r = RingtoneManager.getRingtone(applicationContext, notification)
        //r.play()
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent) {
        if (Util.shouldIgnoreEvent(event)) {
            return
        }

        Log.i("Xiaoyi", AccessibilityEvent.eventTypeToString(event.eventType))

        when (event.eventType) {
            AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED -> onScreenChanged(this, event)
            AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED -> onContentChanged(this, event)
        }
    }

    override fun onKeyEvent(event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_UP && event.keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            captureScreen(this)
            return true
        }
        return true
    }


    override fun onInterrupt() {}
}
