package xiaoyiz.InteractionProxies.Utils

import android.content.Context
import android.graphics.Color
import android.graphics.PixelFormat
import android.graphics.Rect
import android.view.Gravity
import android.view.ViewGroup
import android.view.WindowManager
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.Button
import android.widget.LinearLayout
import xiaoyiz.InteractionProxies.Identifier.WindowTreeNode
import xiaoyiz.InteractionProxies.R
import android.R.attr.button
import android.accessibilityservice.AccessibilityService
import android.util.Log
import android.view.View
import android.view.accessibility.AccessibilityWindowInfo


class Overlay {
    var accessibilityNode: AccessibilityNodeInfo
    var overlay: LinearLayout
    var correctLabelButton: Button
    var bounds = Rect()

    constructor(context: Context,
                accessibilityNode: AccessibilityNodeInfo,
                label: String) {
        overlay = LinearLayout(context)
        overlay.orientation = LinearLayout.VERTICAL
        overlay.setBackgroundColor(Color.argb(150, 0, 180, 0))
        overlay.alpha = 0.3.toFloat()

        this.accessibilityNode = accessibilityNode

        this.accessibilityNode.getBoundsInScreen(bounds)

        val params = WindowManager.LayoutParams(
                bounds.width(),
                bounds.height(),
                bounds.left,
                bounds.top - Util.statusBarHeight,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT)
        params.gravity = Gravity.TOP or Gravity.LEFT
        Util.windowManager.addView(overlay, params)
        Util.overlayList.add(overlay)

        val prev = LinearLayout(context)
        prev.isFocusable = true
        prev.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1)
        prev.setBackgroundColor(Color.argb(150, 0, 0, 180))
        prev.setPadding(0, 0, 0, 0)

        val next = LinearLayout(context)
        next.isFocusable = true
        next.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2)
        next.setBackgroundColor(Color.argb(150, 180, 0, 0))
        next.setPadding(0, 0, 0, 0)

        correctLabelButton = Button(context)
        correctLabelButton.setPadding(0, 0, 0, 0)
        correctLabelButton.text = label
        correctLabelButton.setBackgroundColor(Color.TRANSPARENT)
        correctLabelButton.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, bounds.height() - 3)

        /*
        correctLabelButton.setOnClickListener({ view ->
            Util.windowManager.removeView(overlay)
            (context as AccessibilityService).dispatchGesture(Util.gestureOneFingerClick(bounds.centerX(), bounds.centerY(), 100), null, null)
        })
        */

        overlay.addView(prev)
        overlay.addView(correctLabelButton)
        overlay.addView(next)
    }
}