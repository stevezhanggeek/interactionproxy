package xiaoyiz.InteractionProxies.Identifier

import android.graphics.Rect
import android.util.Log
import android.view.accessibility.AccessibilityNodeInfo

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.database.ValueEventListener
import xiaoyiz.InteractionProxies.Utils.Util

import java.util.ArrayList

class WindowTreeNode {
    var id = ""

    // TODO: maybe initialize as null so we can find difference between "" and null?
    var className: String? = null
    var text: String? = null
    var contentDescription: String? = null
    var viewIdResName: String? = null

    var isLeaf = false
    var depth = -1
    var parentIndex = -1
    var childrenIndexList: ArrayList<Int>? = ArrayList()

    var anchorNodeId: String? = null
    var talkbackText : String? = null

    // TODO: maintain DBs for different resolutions
    // Now we only upload it to firebase(to crop item image), but not download it.
    var boundsInScreen = Rect()
    //public Rect boundsInParent = new Rect();


    // Only used to determine parent/children relationship for now.
    var accessibilityNode: AccessibilityNodeInfo? = null

    // Create from client
    constructor(node: AccessibilityNodeInfo, depth: Int) {
        if (id != "") return

        if (node.className != null) {
            this.className = node.className.toString()
        }
        if (node.text != null) {
            this.text = node.text.toString()
        }
        if (node.contentDescription != null) {
            this.contentDescription = node.contentDescription.toString()
        }
        if (node.viewIdResourceName != null) {
            this.viewIdResName = node.viewIdResourceName.toString()
        }

        if (node.childCount == 0) {
            this.isLeaf = true
        }
        this.depth = depth

        //node.getBoundsInParent(this.boundsInParent);
        node.getBoundsInScreen(this.boundsInScreen)

        accessibilityNode = node
        this.id = "" + -accessibilityNode!!.hashCode()
    }

    // Download from Firebase
    constructor(treeId: String, nodeId: String) {
        this.id = nodeId

        val self = this
        val ref = FirebaseDatabase.getInstance().reference.child(Util.WindowTreeNodeDB).child(treeId).child(nodeId)
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                self.className = dataSnapshot.child("className").value as String?
                self.text = dataSnapshot.child("text").value as String?
                self.contentDescription = dataSnapshot.child("contentDescription").value as String?
                self.viewIdResName = dataSnapshot.child("viewIdResName").value as String?

                self.isLeaf = dataSnapshot.child("isLeaf").value as Boolean
                self.depth = (dataSnapshot.child("depth").value as Long).toInt()
                self.parentIndex = (dataSnapshot.child("parentIndex").value as Long).toInt()
                self.childrenIndexList = dataSnapshot.child("childrenIndexList").getValue(object : GenericTypeIndicator<ArrayList<Int>>() {
                })
                if (self.childrenIndexList == null) {
                    self.childrenIndexList = ArrayList<Int>()
                }

                self.anchorNodeId = dataSnapshot.child("anchorNodeId").value as String?
                self.talkbackText = dataSnapshot.child("talkbackText").value as String?
            }

            override fun onCancelled(firebaseError: DatabaseError) {

            }
        })
    }

    fun writeToFirebase(windowTreeId: String) {
        val ref = FirebaseDatabase.getInstance().reference.child(Util.WindowTreeNodeDB).child(windowTreeId).child(this.id)

        ref.child("className").setValue(className)
        ref.child("text").setValue(text)
        ref.child("contentDescription").setValue(contentDescription)
        ref.child("viewIdResName").setValue(viewIdResName)

        ref.child("isLeaf").setValue(isLeaf)
        ref.child("depth").setValue(depth)
        ref.child("parentIndex").setValue(parentIndex)
        ref.child("childrenIndexList").setValue(childrenIndexList)

        ref.child("boundsInScreen").setValue(boundsInScreen)

        ref.child("anchorNodeId").setValue(anchorNodeId)
        ref.child("talkbackText").setValue(talkbackText)
    }

    fun printData() {
        Log.i("Xiaoyi", "" + this.className + ", " + this.text + ", " + this.contentDescription + ", " + this.viewIdResName)
    }
}
