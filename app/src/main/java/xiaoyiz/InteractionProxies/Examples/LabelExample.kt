package xiaoyiz.InteractionProxies.Examples

import android.accessibilityservice.AccessibilityService
import android.graphics.Rect
import android.util.Log
import android.view.KeyEvent
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo

import xiaoyiz.InteractionProxies.Identifier.WindowTree

import xiaoyiz.InteractionProxies.Utils.*


class LabelExample: AccessibilityService() {
    private var labelHelper = LabelHelper()
    private val windowTreeId = "1500857906316"

    override fun onCreate() {
        super.onCreate()

        Util.setup(this)
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent) {
        when (event.eventType) {
            AccessibilityEvent.TYPE_TOUCH_EXPLORATION_GESTURE_START -> {
                labelHelper.inSwipeNavigationMode = false
                Log.i("Xiaoyi", "TYPE_TOUCH_EXPLORATION_GESTURE_START")
            }
            AccessibilityEvent.TYPE_TOUCH_EXPLORATION_GESTURE_END -> {
                labelHelper.inSwipeNavigationMode = true
                Log.i("Xiaoyi", "TYPE_TOUCH_EXPLORATION_GESTURE_END");
            }
            AccessibilityEvent.TYPE_TOUCH_INTERACTION_START -> {
                Log.i("Xiaoyi", "TYPE_TOUCH_INTERACTION_START");
            }
            AccessibilityEvent.TYPE_TOUCH_INTERACTION_END -> {
                Log.i("Xiaoyi", "TYPE_TOUCH_INTERACTION_END");
            }
        }

        if (shouldIgnoreEvent(event)) {
            return
        }

        val context = this

        if (event.packageName == Util.proxyPackageName) {
            if (event.eventType == AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED) {
                val bounds = Rect()
                event.source.getBoundsInScreen(bounds)

                // Focus on correctLabelButton
                if (bounds.height() > 2) {
                    for (overlay in labelHelper.overlayList) {
                        // The source text might be all upper case.
                        if (overlay.correctLabelButton.text.toString().toLowerCase() == event.source.text.toString().toLowerCase()) {
                            labelHelper.focusedNode = overlay.accessibilityNode
                        }
                    }
                    return
                }

                // Focus on prev/next helper button
                if (labelHelper.focusedNode != null) {
                    if (labelHelper.shouldReturnToAnchor) {
                        labelHelper.anchorNode!!.performAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS)
                    } else {
                        labelHelper.focusedNode!!.performAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS)
                        if (bounds.height() == 1) {
                            dispatchGesture(Util.leftSwipe(), null, null)
                        } else if (bounds.height() == 2) {
                            dispatchGesture(Util.rightSwipe(), null, null)
                        }
                    }
                }
            }

            return
        }


        when (event.eventType) {
            AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED -> {
                if (event.packageName != Util.proxyPackageName) {
                    if (event.source == null) { return }

                    // Switch focus from anchor node to overlay when in swipe navigation mode
                    // Do nothing if in touch exploration mode
                    if (labelHelper.inSwipeNavigationMode) {
                        // Now let's assume user will use EITHER touch exploration OR swipe navigation, not hybrid.
                        // TODO: handle the case when user touches overlay and then swipes. Need to determine prev/next node.
                        if (labelHelper.shouldReturnToAnchor) {
                            labelHelper.shouldReturnToAnchor = false
                            return
                        }

                        if (event.source.viewIdResourceName == "com.yelp.android:id/open_closed") {
                            for (overlay in labelHelper.overlayList) {
                                if (overlay.accessibilityNode.viewIdResourceName == "com.yelp.android:id/war_stars_view") {
                                    labelHelper.shouldReturnToAnchor = true
                                    labelHelper.anchorNode = event.source
                                    overlay.correctLabelButton.performAccessibilityAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS, null)
                                    return
                                }
                            }
                        }
                    } else {
                        labelHelper.shouldReturnToAnchor = false
                    }

                    // Find the proxy that above this unlabeled item, switch focus to proxy.
                    for (overlay in labelHelper.overlayList) {
                        if (overlay.accessibilityNode == event.source) {
                            labelHelper.focusedNode = event.source
                            overlay.correctLabelButton.performAccessibilityAction(AccessibilityNodeInfo.ACTION_ACCESSIBILITY_FOCUS, null)
                        }
                    }
                }
            }

            AccessibilityEvent.TYPE_VIEW_CLICKED -> {
            }

            AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED -> {
            }

            AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED -> {
                labelHelper.removeAllOverlays(context)

                val activityName = Util.getActivityName(context, event)
                labelHelper = LabelHelper()
                labelHelper.remoteWindowTree = WindowTree(windowTreeId)

                android.os.Handler().postDelayed(
                        {
                            if (rootInActiveWindow != null) {
                                labelHelper.clientWindowTree = WindowTree(rootInActiveWindow, activityName, context)
                                val addedLabelList = labelHelper.remoteWindowTree!!.addedLabelList
                                for (addedLabel in addedLabelList) {
                                    for (remoteNode in labelHelper.remoteWindowTree!!.windowTreeNodeList) {
                                        if (remoteNode.id == addedLabel.first) {
                                            var node = getNodeOnScreen(labelHelper.clientWindowTree!!, remoteNode)
                                            if (node != null) {
                                                labelHelper.overlayList.add(Overlay(context, node.accessibilityNode!!, addedLabel.second))
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        2000)
            }
        }
    }


    override fun onKeyEvent(event: KeyEvent): Boolean {
        return true
    }


    override fun onInterrupt() {}
}