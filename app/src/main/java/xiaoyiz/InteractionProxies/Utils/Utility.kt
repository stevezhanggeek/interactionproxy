package xiaoyiz.InteractionProxies.Utils

import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import xiaoyiz.InteractionProxies.TalkbackUtils.AccessibilityNodeInfoUtils
import xiaoyiz.InteractionProxies.TalkbackUtils.OrderedTraversalController
import xiaoyiz.InteractionProxies.TalkbackUtils.ReorderedChildrenIterator
import java.util.ArrayList
import java.util.HashSet

fun isImportantNode(node: AccessibilityNodeInfo): Boolean {
    if (node.viewIdResourceName != null) {
        return true
    }

    if (node.text.isNotEmpty() || node.contentDescription.isNotEmpty()) {
        return true
    }

    if (node.className.contains("ImageView")
     || node.className.contains("TextView")
     || node.className.contains("ImageView")
     || node.className.contains("ImageView")) {
        return true
    }

   return false
}

/**
 * Obtain a list of nodes in the order TalkBack would traverse them
 *
 * @param root The root of the tree to traverse
 * @return The nodes on screen in the order TalkBack would traverse them.
 */
fun getNodesInTalkBackOrder(root: AccessibilityNodeInfoCompat): ArrayList<AccessibilityNodeInfoCompat> {
    val outList = ArrayList<AccessibilityNodeInfoCompat>()
    val traversalController = OrderedTraversalController()
    traversalController.initOrder(root, true)
    var node = traversalController.findFirst()
    while (node != null) {
        // When it is focusable(speakable) by Talkback
        if (AccessibilityNodeInfoUtils.shouldFocusNode(node, null)) {
            outList.add(node)
        }
        node = traversalController.findNext(node)
    }
    traversalController.recycle()

    return outList
}

fun getAllNodes(info: AccessibilityNodeInfoCompat): ArrayList<AccessibilityNodeInfoCompat> {
    val allNodeList = ArrayList<AccessibilityNodeInfoCompat>()
    getAllNodesHelper(allNodeList, info)
    return allNodeList
}

fun getAllNodesHelper(unlabeledNodeList: ArrayList<AccessibilityNodeInfoCompat>,
                      info: AccessibilityNodeInfoCompat?) {
    if (info == null) return
    if (info.childCount == 0) {
        unlabeledNodeList.add(info)
    } else {
        unlabeledNodeList.add(info)
        for (i in 0..info.childCount - 1) {
            getAllNodesHelper(unlabeledNodeList, info.getChild(i))
        }
    }
}