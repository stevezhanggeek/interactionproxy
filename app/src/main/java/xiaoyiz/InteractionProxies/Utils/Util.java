package xiaoyiz.InteractionProxies.Utils;

import android.accessibilityservice.GestureDescription;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Path;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjectionManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import static android.content.Context.WINDOW_SERVICE;

public class Util {
    public enum ScreenReader {
        TALKBACK,
        SHINEPLUS
    }

    public static String proxyPackageName = "xiaoyiz.InteractionProxies";
    public static String AppIdDB = "AppIdDB_Test";
    public static String WindowTreeDB = "WindowTreeDB_Test";
    public static String WindowTreeNodeDB = "WindowTreeNodeDB_Test";

    public static int statusBarHeight = 0;
    public static WindowManager windowManager;
    public static ArrayList<View> overlayList = new ArrayList<>();
    public static ScreenReader screenReader = ScreenReader.TALKBACK;

    public static MediaProjectionManager mediaProjectionManager;
    public static ImageReader imageReader;
    public static VirtualDisplay virtualDisplay;
    public static int screenW = 0;
    public static int screenH = 0;


    public static void setup(Context context) {
        Util.statusBarHeight = Util.getStatusBarHeight(context);
        Util.windowManager   = (WindowManager) context.getSystemService(WINDOW_SERVICE);
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static ArrayList<AccessibilityNodeInfo> getAllUnlabeledNodes(AccessibilityNodeInfo info) {
        ArrayList<AccessibilityNodeInfo> unlabeledNodeList = new ArrayList<>();
        Util.getAllUnlabeledNodesHelper(unlabeledNodeList, info);
        return unlabeledNodeList;
    }

    public static void getAllUnlabeledNodesHelper(ArrayList<AccessibilityNodeInfo> unlabeledNodeList,
                                                  AccessibilityNodeInfo info) {
        if (info == null) return;
        if (info.getChildCount() == 0 && (info.getText() == null && info.getContentDescription() == null)) {
            unlabeledNodeList.add(info);
        } else {/*
            if (info.getText() == null && info.getContentDescription() == null) {
                unlabeledNodeList.add(info);
            }*/
            for (int i = 0; i < info.getChildCount(); i++) {
                getAllUnlabeledNodesHelper(unlabeledNodeList, info.getChild(i));
            }
        }
    }

    public static ArrayList<String> getAllViewIdResourceName(AccessibilityNodeInfo info) {
        ArrayList<String> viewIdResourceNameList = new ArrayList<>();
        Util.getAllViewIdResourceNameHelper(viewIdResourceNameList, info);
        return viewIdResourceNameList;
    }

    public static void getAllViewIdResourceNameHelper(ArrayList<String> viewIdResourceNameList,
                                                      AccessibilityNodeInfo info) {
        if (info == null) return;
        if (info.getViewIdResourceName() != null) {
            viewIdResourceNameList.add(info.getViewIdResourceName());
        }
        if (info.getChildCount() != 0) {
            for (int i = 0; i < info.getChildCount(); i++) {
                getAllViewIdResourceNameHelper(viewIdResourceNameList, info.getChild(i));
            }
        }
    }

    public static GestureDescription leftSwipe() {
        Path leftSwipe = new Path();
        leftSwipe.moveTo(400, 100);
        leftSwipe.lineTo(100, 100);

        GestureDescription.Builder builder = new GestureDescription.Builder();
        builder.addStroke(new GestureDescription.StrokeDescription(leftSwipe, 0, 1));

        return builder.build();
    }

    public static GestureDescription rightSwipe() {
        Path rightSwipe = new Path();
        rightSwipe.moveTo(100, 100);
        rightSwipe.lineTo(400, 100);

        GestureDescription.Builder builder = new GestureDescription.Builder();
        builder.addStroke(new GestureDescription.StrokeDescription(rightSwipe, 0, 1));

        return builder.build();
    }

    public static GestureDescription gestureOneFingerClick(int center_x, int center_y, int delay_ms){
        Path scroll = new Path();
        scroll.moveTo(center_x, center_y);
        scroll.lineTo(center_x, center_y);

        GestureDescription.StrokeDescription stroke = new GestureDescription.StrokeDescription(scroll, delay_ms, 10);
        GestureDescription.Builder gesture_builder = new GestureDescription.Builder();
        gesture_builder.addStroke(stroke);
        GestureDescription gest_descript = gesture_builder.build();

        return gest_descript;
    }

    public static GestureDescription gestureTwoFingerClick(int center_x, int center_y, int delay_ms){
        Path scroll = new Path();
        scroll.moveTo(center_x-3, center_y);
        scroll.lineTo(center_x-3, center_y+5);

        Path scroll2 = new Path();
        scroll2.moveTo(center_x + 3, center_y - 3);

        GestureDescription.StrokeDescription stroke = new GestureDescription.StrokeDescription(scroll, delay_ms, 300);
        GestureDescription.StrokeDescription stroke2 = new GestureDescription.StrokeDescription(scroll2, delay_ms, 300);
        GestureDescription.Builder gesture_builder = new GestureDescription.Builder();
        gesture_builder.addStroke(stroke);
        gesture_builder.addStroke(stroke2);
        GestureDescription gest_descript = gesture_builder.build();

        return gest_descript;
    }

    public static int getVersionCode(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(packageName, 0);
            //Log.i("Xiaoyi", "VersionName: "+info.versionName);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static boolean shouldIgnoreEvent(AccessibilityEvent event) {
        if (event.getPackageName() == null) {
            return true;
        }
        if (event.getPackageName().toString().contains("com.google.android")
                ||event.getPackageName().toString().contains("com.android")
                ||event.getPackageName().toString().equals("android")) {
            return true;
        }
        if (event.getPackageName().equals(proxyPackageName)) {
            return true;
        }
        return false;
    }


    public static String getActivityName(Context context, AccessibilityEvent event) {
        if (event.getPackageName() != null && event.getClassName() != null) {
            ComponentName componentName = new ComponentName(
                    event.getPackageName().toString(),
                    event.getClassName().toString()
            );
            try {
                // If it is an activity, className is activityName
                if (context.getPackageManager().getActivityInfo(componentName, 0) != null) {
                    return componentName.getClassName();
                }
            } catch (PackageManager.NameNotFoundException e) {
                return null;
            }
        }
        return null;
    }

    public static byte[] getScreenShot() {
        if (imageReader == null) return null;
        final Image image = imageReader.acquireLatestImage();
        if (image == null || image.getPlanes() == null) return null;

        Image.Plane plane = image.getPlanes()[0];
        if (plane == null) return null;

        final ByteBuffer buffer = plane.getBuffer();
        final int pixelStride = plane.getPixelStride();
        final int rowPadding = plane.getRowStride() - pixelStride * screenW;

        final Bitmap bmp = Bitmap.createBitmap(screenW + rowPadding / pixelStride, screenH, Bitmap.Config.ARGB_8888);
        bmp.copyPixelsFromBuffer(buffer);
        image.close();

        final Bitmap croppedBmp = Bitmap.createBitmap(bmp, 0, 0, screenW, screenH);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        croppedBmp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        return stream.toByteArray();
    }

    public static String formatForFirebase(String packageName) {
        return packageName.replace('.', '-');
    }
}
